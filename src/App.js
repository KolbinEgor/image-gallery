import React, { Component } from "react";
import _ from "lodash";
import "./App.css";
import shortid from "shortid";
import GalleryItem from "./components/GalleryItem";
import MyButton from "./components/MyButton";
import Popup from "./components/Popup";

const ImagesList = [
  {
    Title: "Flower",
    URL: "./Flower.jpg",
    selected: false
  },
  {
    Title: "Mountain",
    URL: "./Mountain.jpeg",
    selected: false
  }
];

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ImagesList: ImagesList,
      showPopup: false
    };
  }

  newImage = (Title, URL) => {
    let ImagesList = _.cloneDeep(this.state.ImagesList);
    ImagesList.push({ Title: Title, URL: URL, selected: false });
    this.setState({ ImagesList });
    this.closePopup();
  };
  dropImg = URL => {
    let ImagesList = _.cloneDeep(this.state.ImagesList);
    _.remove(ImagesList, function(Image) {
      return Image.URL === URL;
    });
    this.setState({ ImagesList });
  };

  selectImage = URL => {
    let ImagesList = _.cloneDeep(this.state.ImagesList);
    let indexSelected = _.findIndex(ImagesList, { selected: true });
    let indexClicked = _.findIndex(ImagesList, { URL: URL });

    if (indexSelected >= 0 && indexSelected !== indexClicked) {
      let SelectedImage = _.cloneDeep(ImagesList[indexSelected]);
      SelectedImage.selected = false;
      ImagesList.splice(indexSelected, 1, SelectedImage);
    }

    let ClickedImage = _.cloneDeep(ImagesList[indexClicked]);
    ClickedImage.selected = !ClickedImage.selected;
    ImagesList.splice(indexClicked, 1, ClickedImage);

    this.setState({ ImagesList });
  };
  onNewClick = () => {
    this.setState({
      showPopup: true
    });
  };
  closePopup = () => {
    this.setState({
      showPopup: false
    });
  };
  render() {
    return (
      <div className="App">
        {this.state.showPopup ? (
          <Popup
            text="Close Me"
            closePopup={this.closePopup}
            newImage={this.newImage}
          />
        ) : null}
        <header className="App-header">
          <span className="App-headerBall" />
          Images
        </header>
        <div className="Gallery">
          <MyButton text="NEW" onClick={this.onNewClick} />
          {this.state.ImagesList.map(Image => (
            <GalleryItem
              key={shortid.generate()}
              src={Image.URL}
              Title={Image.Title}
              selected={Image.selected}
              onDelete={this.dropImg}
              selectImg={this.selectImage}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default App;
