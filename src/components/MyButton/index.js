import React, { Component } from "react";
import "./MyButton.css";

class MyButton extends Component {
  render() {
    let FullClass = "button--hollow ";
    if (this.props.textcolor) FullClass += this.props.textcolor;

    return (
      <button
        onClick={this.props.onClick}
        className={FullClass}
        disabled={this.props.disabled}
      >
        {this.props.text}
      </button>
    );
  }
}

export default MyButton;
