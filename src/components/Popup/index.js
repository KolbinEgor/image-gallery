import React, { Component } from "react";
import MyButton from "../../components/MyButton";
import "./Popup.css";

class Popup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Title: "",
      URL: "",
      validTitle: false,
      validURL: false,
      validForm: false
    };
  }

  ADDClick = () => {
    this.props.newImage(this.state.Title, this.state.URL);
  };

  InputChange = e => {
    let inputName = e.target.name;
    let value = e.target.value;
    let CurValidValue = value.length > 2 ? true : false;
    let CurValidName = inputName === "Title" ? "validTitle" : "validURL";
    this.setState(
      {
        [inputName]: value,
        [CurValidName]: CurValidValue
      },
      this.validateInputs
    );
  };

  validateInputs = () => {
    let validForm = this.state.validTitle && this.state.validURL;
    if (this.state.validForm !== validForm) this.setState({ validForm });
  };

  render() {
    return (
      <div className="popup">
        <div className="popup_inner">
          <div className="popup_innerSectionTitle">
            <div>New image</div>
          </div>
          <div className="popup_innerSectionBody">
            <div className="popup_innerSectionInputs">
              <div>
                <input
                  name="Title"
                  placeholder="Title"
                  onChange={this.InputChange}
                />
              </div>
              <div>
                <input
                  name="URL"
                  placeholder="URL"
                  onChange={this.InputChange}
                />
              </div>
            </div>
            <div className="popup_innerSectionBattons">
              <MyButton
                text="CLOSE"
                onClick={this.props.closePopup}
                textcolor="button--black"
              />
              <MyButton
                text="ADD"
                onClick={this.ADDClick}
                disabled={!this.state.validForm}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Popup;
