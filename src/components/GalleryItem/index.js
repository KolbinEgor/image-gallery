import React, { Component } from "react";
import classNames from "classnames";
import "./GalleryItem.css";

class GalleryItem extends Component {
  DeleteClicked = e => {
    e.stopPropagation();
    this.props.onDelete(this.props.src);
  };
  ImgClicked = () => {
    this.props.selectImg(this.props.src);
  };
  render() {
    return (
      <div
        className={classNames("GalleryItem", { Selected: this.props.selected })}
      >
        <div className="GalleryItemModal">
          <div className="GalleryItemModalSpan" onClick={this.ImgClicked}>
            <div
              className="GalleryItemModalSpanDelete"
              onClick={this.DeleteClicked}
            >
              Delete
            </div>
          </div>
        </div>
        <div className="GalleryItemHeader">
          <div className="GalleryItemTitle">{this.props.Title} </div>
          <div className="GalleryItemDelete">
            <span
              className="GalleryItemDeleteCursor"
              onClick={this.DeleteClicked}
            >
              Delete
            </span>
          </div>
        </div>
        <div className="GalleryItemImgBox">
          <img
            className="GalleryItemImg"
            src={this.props.src}
            onClick={this.ImgClicked}
          />
        </div>
      </div>
    );
  }
}

export default GalleryItem;
